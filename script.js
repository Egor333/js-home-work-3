const userInput = parseInt(prompt("Введіть число:"));

if (isNaN(userInput)) {
  console.log("Invalid input. Please enter a valid number.");
} else {
  for (let i = 0; i <= userInput; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }

  if (userInput <= 4) {
    console.log("Sorry, no numbers.");
  }
}
